
plugins {
    id(Dependencies.Plugins.androidApplication) version Dependencies.Plugins.androidApplicationVersion apply false
    id(Dependencies.Plugins.androidLibrary) version Dependencies.Plugins.androidLibraryVersion apply false
    id(Dependencies.Plugins.kotlinAndroid) version Dependencies.Plugins.kotlinAndroidVersion apply false
    id(Dependencies.Plugins.hilt) version Dependencies.Plugins.hiltVersion apply false
}

//task clean(type: Delete) {
//    delete rootProject.buildDir
//}