object Dependencies {

    object Compose {

        const val composeVersion = "1.1.1"
        const val navigationVersion = "2.4.1"

        const val ui = "androidx.compose.ui:ui:$composeVersion"
        const val material = "androidx.compose.material:material:$composeVersion"
        const val toolingPreview = "androidx.compose.ui:ui-tooling-preview:$composeVersion"
        const val junitTest = "androidx.compose.ui:ui-test-junit4:$composeVersion"
        const val uiTooling = "androidx.compose.ui:ui-tooling:$composeVersion"

        const val navigation = "androidx.navigation:navigation-compose:$navigationVersion"
        const val hiltNavigation = "androidx.hilt:hilt-navigation-compose:1.0.0"
    }

    object Core {
        private const val ktxVersion = "1.7.0"
        const val ktx = "androidx.core:core-ktx:$ktxVersion"
    }

    object Lifecycle {
        private const val lifecycleVersion = "2.4.1"
        const val runtime = "androidx.lifecycle:lifecycle-runtime-ktx:$lifecycleVersion"
        const val viewModel = "androidx.lifecycle:lifecycle-viewmodel-ktx:$lifecycleVersion"
        const val lifeData = "androidx.lifecycle:lifecycle-livedata-ktx:$lifecycleVersion"
        const val viewModelCompose = "androidx.lifecycle:lifecycle-viewmodel-compose:$lifecycleVersion"
    }

    object Activity {
        private const val activityVersion = "1.4.0"
        const val compose = "androidx.activity:activity-compose:$activityVersion"
    }

    object Room {
        private const val roomVersion = "2.4.2"
        const val runtime = "androidx.room:room-runtime:$roomVersion"
        const val compiler = "androidx.room:room-compiler:$roomVersion"
        const val ktx = "androidx.room:room-ktx:$roomVersion"
    }

    object Hilt {
        const val runtime = "com.google.dagger:hilt-android:${Plugins.hiltVersion}"
        const val compiler = "com.google.dagger:hilt-android-compiler:${Plugins.hiltVersion}"

        const val extensionVersion = "1.3.0"
        const val extensionRuntime = "it.czerwinski.android.hilt:hilt-extensions:$extensionVersion"
        const val extensionCompiler = "it.czerwinski.android.hilt:hilt-processor:$extensionVersion"
    }

    object Plugins {
        const val androidApplicationVersion = "7.1.2"
        const val androidApplication = "com.android.application"

        const val androidLibraryVersion = "7.1.2"
        const val androidLibrary = "com.android.library"

        const val kotlinAndroidVersion = "1.6.10"
        const val kotlinAndroid = "org.jetbrains.kotlin.android"

        const val kapt = "kapt"

        const val hiltVersion = "2.41"
        const val hilt = "dagger.hilt.android.plugin"
    }
}