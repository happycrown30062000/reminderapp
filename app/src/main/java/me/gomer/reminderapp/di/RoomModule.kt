package me.gomer.reminderapp.di

import android.app.Application
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import me.gomer.reminderapp.data.local.NotesDao
import me.gomer.reminderapp.data.local.NotesDatabase
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RoomModule {

    @Provides
    @Singleton
    fun providesNotesDatabase(application: Application): NotesDatabase =
        Room.databaseBuilder(
            application,
            NotesDatabase::class.java,
            NotesDatabase.DATABASE_NAME
        ).build()

    @Provides
    @Singleton
    fun provideNotesDao(database: NotesDatabase): NotesDao =
        database.notesDao
}