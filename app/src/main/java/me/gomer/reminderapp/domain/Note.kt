package me.gomer.reminderapp.domain

data class Note(
    val id: Int,
    val title: String,
    val body: String,
    val timestamp: Long
)
