package me.gomer.reminderapp.domain

enum class Order {
    ASCENDING,
    DESCENDING
}