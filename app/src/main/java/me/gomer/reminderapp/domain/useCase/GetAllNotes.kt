package me.gomer.reminderapp.domain.useCase

import me.gomer.reminderapp.domain.DataStatus
import me.gomer.reminderapp.domain.Note
import me.gomer.reminderapp.domain.Order
import me.gomer.reminderapp.domain.repository.NotesRepository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GetAllNotes @Inject constructor(
    private val notesRepository: NotesRepository
) {

    suspend operator fun invoke(order: Order): DataStatus<List<Note>> =
        notesRepository.getAllNotes(order = order)

}