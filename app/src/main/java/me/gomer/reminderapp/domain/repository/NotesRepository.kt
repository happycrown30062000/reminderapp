package me.gomer.reminderapp.domain.repository

import me.gomer.reminderapp.domain.DataStatus
import me.gomer.reminderapp.domain.Note
import me.gomer.reminderapp.domain.Order

interface NotesRepository {

    suspend fun addNote(note: Note)
    suspend fun deleteNode(note: Note)
    suspend fun getNote(id: Int): DataStatus<Note>
    suspend fun getAllNotes(order: Order): DataStatus<List<Note>>

}