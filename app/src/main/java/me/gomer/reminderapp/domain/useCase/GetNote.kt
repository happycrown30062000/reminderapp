package me.gomer.reminderapp.domain.useCase

import me.gomer.reminderapp.domain.DataStatus
import me.gomer.reminderapp.domain.Note
import me.gomer.reminderapp.domain.repository.NotesRepository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GetNote @Inject constructor(
    private val notesRepository: NotesRepository
) {

    suspend operator fun invoke(id: Int): DataStatus<Note> =
        notesRepository.getNote(id = id)

}