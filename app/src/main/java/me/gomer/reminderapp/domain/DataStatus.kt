package me.gomer.reminderapp.domain

sealed class DataStatus<out T> {

    object Idle: DataStatus<Nothing>()
    object Loading: DataStatus<Nothing>()
    data class Success<out R>(val data: R): DataStatus<R>()
    data class Error(val errorType: ErrorType): DataStatus<Nothing>()

}
