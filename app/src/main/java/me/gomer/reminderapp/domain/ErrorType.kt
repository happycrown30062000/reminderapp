package me.gomer.reminderapp.domain

enum class ErrorType {
    INTERNET,
    UNKNOWN
}