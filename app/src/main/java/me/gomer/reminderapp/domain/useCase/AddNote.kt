package me.gomer.reminderapp.domain.useCase

import me.gomer.reminderapp.domain.Note
import me.gomer.reminderapp.domain.repository.NotesRepository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AddNote @Inject constructor(
    private val notesRepository: NotesRepository
) {

    suspend operator fun invoke(note: Note) =
        notesRepository.addNote(note = note)

}