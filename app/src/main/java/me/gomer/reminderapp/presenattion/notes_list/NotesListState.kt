package me.gomer.reminderapp.presenattion.notes_list

import me.gomer.reminderapp.domain.DataStatus
import me.gomer.reminderapp.domain.Note
import me.gomer.reminderapp.domain.Order

data class NotesListState(
    val notesListStatus: DataStatus<List<Note>> = DataStatus.Idle,
    val order: Order = Order.DESCENDING,
    val isOrderSectionVisible: Boolean = false
)
