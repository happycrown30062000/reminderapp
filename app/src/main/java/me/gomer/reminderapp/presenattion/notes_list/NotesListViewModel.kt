package me.gomer.reminderapp.presenattion.notes_list

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import me.gomer.reminderapp.domain.DataStatus
import me.gomer.reminderapp.domain.Note
import me.gomer.reminderapp.domain.Order
import me.gomer.reminderapp.domain.useCase.DeleteNote
import me.gomer.reminderapp.domain.useCase.GetAllNotes
import javax.inject.Inject

@HiltViewModel
class NotesListViewModel @Inject constructor(
    private val getAllNotes: GetAllNotes,
    private val deleteNote: DeleteNote
): ViewModel() {

    private val _notesListState = mutableStateOf(NotesListState())
    val notesListState: State<NotesListState> = _notesListState

    init {
        loadNotes()
    }

    fun processEvent(event: NotesListEvent) {
        when(event) {
            is NotesListEvent.ChangeOrder -> {
                _notesListState.value = _notesListState.value.copy(
                    order = event.order
                )
            }
            is NotesListEvent.ToggleOrderVisibility -> {
                _notesListState.value = _notesListState.value.copy(
                    isOrderSectionVisible = !_notesListState.value.isOrderSectionVisible
                )
            }
            is NotesListEvent.DeleteNote -> {
                viewModelScope.launch {
                    deleteNote(note = event.note)
                    loadNotes()
                }
            }
        }
    }

    private fun loadNotes() {
        viewModelScope.launch {
            _notesListState.value = _notesListState.value.copy(
                notesListStatus = DataStatus.Loading
            )
            val notes = getAllNotes(order = _notesListState.value.order)
            _notesListState.value = _notesListState.value.copy(
                notesListStatus = notes
            )
        }
    }

    sealed class NotesListEvent {
        data class ChangeOrder(val order: Order): NotesListEvent()
        object ToggleOrderVisibility: NotesListEvent()
        data class DeleteNote(val note: Note): NotesListEvent()
    }
}