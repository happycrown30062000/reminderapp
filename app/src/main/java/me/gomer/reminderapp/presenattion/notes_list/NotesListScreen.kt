package me.gomer.reminderapp.presenattion.notes_list

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import me.gomer.reminderapp.R
import me.gomer.reminderapp.domain.Order

@Preview
@Composable
fun NotesListScreen(notesListViewModel: NotesListViewModel = viewModel()) {

    val state = notesListViewModel.notesListState

    Box(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp),
        contentAlignment = Alignment.BottomEnd
    ) {
        Column(modifier = Modifier.fillMaxSize()) {
            NotesHeader(
                modifier = Modifier.fillMaxWidth(),
                onIconClicked = {
                    notesListViewModel.processEvent(
                        event = NotesListViewModel.NotesListEvent.ToggleOrderVisibility
                    )
                }
            )
            Spacer(modifier = Modifier.height(8.dp))
            AnimatedVisibility(visible = state.value.isOrderSectionVisible) {
                OrderSection(
                    modifier = Modifier.fillMaxWidth(),
                    order = state.value.order
                ) {
                    notesListViewModel.processEvent(
                        event = NotesListViewModel.NotesListEvent.ChangeOrder(order = it)
                    )
                }
            }
        }
        FloatingActionButton(
            onClick = { }
        ) {
            Icon(imageVector = Icons.Default.Add, contentDescription = "Add note")
        }
    }
}

@Composable
fun NotesHeader(
    modifier: Modifier = Modifier,
    onIconClicked: () -> Unit
) {
    Row(
        modifier = modifier,
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically
    ) {
        Text(
            text = stringResource(R.string.notes_header),
            style = MaterialTheme.typography.h5
        )
        Icon(
            modifier = Modifier.clickable {
                onIconClicked()
            },
            painter = painterResource(id = R.drawable.ic_sort),
            contentDescription = "Order visibility",
        )
    }
}

@Composable
fun OrderSection(
    modifier: Modifier = Modifier,
    order: Order,
    onOrderChange: (Order) -> Unit
) {
    Column(modifier = modifier) {
        Text(
            text = stringResource(R.string.order_title),
            style = MaterialTheme.typography.subtitle2
        )
        Row(modifier = Modifier.padding(top = 4.dp)) {
            SimpleRadioButton(
                isSelected = order == Order.ASCENDING,
                hint = "Ascending"
            ) {
                onOrderChange(Order.ASCENDING)
            }
            Spacer(modifier = Modifier.width(4.dp))
            SimpleRadioButton(
                isSelected = order == Order.DESCENDING,
                hint = "Descending"
            ) {
                onOrderChange(Order.DESCENDING)
            }
        }
    }
}

@Composable
fun SimpleRadioButton(
    modifier: Modifier = Modifier,
    isSelected: Boolean,
    hint: String,
    onClick: () -> Unit
) {
    Row(
        modifier = modifier,
        verticalAlignment = Alignment.CenterVertically
    ) {
        RadioButton(
            selected = isSelected,
            onClick = onClick
        )
        Spacer(modifier = Modifier.width(4.dp))
        Text(
            text = hint,
            style = MaterialTheme.typography.overline
        )
    }
}