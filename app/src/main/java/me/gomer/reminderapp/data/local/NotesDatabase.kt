package me.gomer.reminderapp.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import me.gomer.reminderapp.data.NoteEntity

@Database(entities = [NoteEntity::class], version = 1, exportSchema = false)
abstract class NotesDatabase: RoomDatabase() {

    abstract val notesDao: NotesDao

    companion object {
        const val DATABASE_NAME = "notes_database"
    }
}