package me.gomer.reminderapp.data

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "notes")
data class NoteEntity(
    @PrimaryKey(autoGenerate = true)
    val id: Int = DEFAULT_ID,
    val title: String,
    val body: String,
    val timestamp: Long
) {

    companion object {
        const val DEFAULT_ID = -1
    }

}
