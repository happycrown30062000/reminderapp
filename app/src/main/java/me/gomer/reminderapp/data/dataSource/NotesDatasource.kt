package me.gomer.reminderapp.data.dataSource

import me.gomer.reminderapp.data.NoteEntity

interface NotesDatasource {

    suspend fun addNote(noteEntity: NoteEntity)
    suspend fun deleteNote(noteE: NoteEntity)
    suspend fun getNote(id: Int): NoteEntity
    suspend fun getAllNotes(): List<NoteEntity>

}