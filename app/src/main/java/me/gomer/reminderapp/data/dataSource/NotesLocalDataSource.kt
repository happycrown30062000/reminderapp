package me.gomer.reminderapp.data.dataSource

import dagger.hilt.components.SingletonComponent
import it.czerwinski.android.hilt.annotations.BoundTo
import me.gomer.reminderapp.data.NoteEntity
import me.gomer.reminderapp.data.local.NotesDao
import me.gomer.reminderapp.di.Local
import javax.inject.Inject

@Local
@BoundTo(supertype = NotesDatasource::class, component = SingletonComponent::class)
class NotesLocalDataSource @Inject constructor(
    private val notesDao: NotesDao
): NotesDatasource {

    override suspend fun addNote(noteEntity: NoteEntity) =
        notesDao.addNote(note = noteEntity)

    override suspend fun deleteNote(noteE: NoteEntity) =
        notesDao.deleteNote(note = noteE)

    override suspend fun getNote(id: Int): NoteEntity =
        notesDao.getNote(id = id)

    override suspend fun getAllNotes(): List<NoteEntity> =
        notesDao.getAllNotes()
}