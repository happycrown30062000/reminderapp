package me.gomer.reminderapp.data

import me.gomer.reminderapp.domain.Note
import javax.inject.Inject

class NotesMapper @Inject constructor() {

    fun mapToDomain(noteEntity: NoteEntity): Note =
        Note(
            id = noteEntity.id,
            timestamp = noteEntity.timestamp,
            title = noteEntity.title,
            body = noteEntity.body
        )

    fun mapToData(note: Note): NoteEntity =
        NoteEntity(
            id = note.id,
            timestamp = note.timestamp,
            title = note.title,
            body = note.body
        )
}