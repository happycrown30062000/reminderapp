package me.gomer.reminderapp.data.local

import androidx.room.*
import me.gomer.reminderapp.data.NoteEntity

@Dao
interface NotesDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addNote(note: NoteEntity)

    @Delete
    suspend fun deleteNote(note: NoteEntity)

    @Query("SELECT * FROM notes")
    suspend fun getAllNotes(): List<NoteEntity>

    @Query("SELECT * FROM notes WHERE id=:id")
    suspend fun getNote(id: Int): NoteEntity
}