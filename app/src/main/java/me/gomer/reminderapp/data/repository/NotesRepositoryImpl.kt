package me.gomer.reminderapp.data.repository

import dagger.hilt.components.SingletonComponent
import it.czerwinski.android.hilt.annotations.BoundTo
import me.gomer.reminderapp.data.NotesMapper
import me.gomer.reminderapp.data.dataSource.NotesDatasource
import me.gomer.reminderapp.di.Local
import me.gomer.reminderapp.domain.DataStatus
import me.gomer.reminderapp.domain.Note
import me.gomer.reminderapp.domain.Order
import me.gomer.reminderapp.domain.repository.NotesRepository
import javax.inject.Inject

@BoundTo(supertype = NotesRepository::class, component = SingletonComponent::class)
class NotesRepositoryImpl @Inject constructor(
    @Local private val notesLocalDataSource: NotesDatasource,
    private val mapper: NotesMapper
) : NotesRepository {

    override suspend fun addNote(note: Note) =
        notesLocalDataSource.addNote(noteEntity = mapper.mapToData(note = note))

    override suspend fun deleteNode(note: Note) =
        notesLocalDataSource.deleteNote(noteE = mapper.mapToData(note = note))

    override suspend fun getNote(id: Int): DataStatus<Note> =
        DataStatus.Success(
            data = mapper.mapToDomain(
                noteEntity = notesLocalDataSource.getNote(id = id)
            )
        )

    override suspend fun getAllNotes(order: Order): DataStatus<List<Note>> =
        when(order) {
            Order.ASCENDING -> {
                DataStatus.Success(
                    data = notesLocalDataSource.getAllNotes().map { noteEntity ->
                        mapper.mapToDomain(noteEntity = noteEntity)
                    }.sortedBy { note -> note.timestamp }
                )
            }
            Order.DESCENDING -> {
                DataStatus.Success(
                    data = notesLocalDataSource.getAllNotes().map { noteEntity ->
                        mapper.mapToDomain(noteEntity = noteEntity)
                    }.sortedByDescending { note -> note.timestamp }
                )
            }
        }

}